<%-- 
    Document   : Customer
    Created on : May 25, 2018, 4:03:46 AM
    Author     : Puuuuu
--%>

<%@page contentType="text/xml" pageEncoding="UTF-8"%>
<%@taglib   uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml-stylesheet type="text/xsl" href="Customer.xsl"?>
<!DOCTYPE html>
<Customers>
        <c:forEach var="customer" items="${customers}">
        <Customer>
            <Contactname>${customer.contactname}</Contactname>
            <Contacttitle>${customer.contacttitle}</Contacttitle>
            <Companyname>${customer.companyname}</Companyname>
            <Address>${customer.address}</Address>
        <City>${customer.city}</City>
        <Postalcode>${customer.postalcode}</Postalcode>
        <Country>${customer.country}</Country>
        <Phone>${customer.phone}</Phone>
        <Fax>${customer.fax}</Fax>
        </Customer>
    </c:forEach>
</Customers>
