/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import entites.Customer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Puuuuu
 */
public class ProcessingXML {
    public ArrayList readXML(String url) throws ParsingException, ValidityException, IOException {
       nu.xom.Document document = new nu.xom.Builder().build(new File(url + "Customers.xml"));
        Nodes nodes = XQueryUtil.xquery(document, "//Customer");
        ArrayList arrayList = new ArrayList();
        System.out.println(nodes.size());
        for(int i = 0; i < nodes.size();i++){
            Customer pro = new Customer();
            nu.xom.Node node = nodes.get(i);
            pro.setContactname(node.getChild(1).getValue());
            pro.setContacttitle(node.getChild(3).getValue());
            pro.setCompanyname(node.getChild(5).getValue());
            pro.setAddress(node.getChild(7).getValue());
            pro.setCity(node.getChild(9).getValue());
            pro.setPostalcode(Integer.parseInt(node.getChild(11).getValue()));
            pro.setCountry(node.getChild(13).getValue());
            pro.setPhone(Integer.parseInt(node.getChild(15).getValue()));
            pro.setFax(Integer.parseInt(node.getChild(17).getValue()));
            arrayList.add(pro);
        }
        return arrayList;
    }

    public ArrayList showContent(String url) throws ParsingException, ValidityException, IOException {
        nu.xom.Document document = new nu.xom.Builder().build(new File(url + "Customers.xml"));
        nu.xom.Nodes nodes = XQueryUtil.xquery(document, "//Customer[City='LonDon']");
        ArrayList arrayList = new ArrayList();
        for(int i =0; i<nodes.size(); i++){
            Customer pro = new Customer();
            nu.xom.Node node = nodes.get(i);
           pro.setContactname(node.getChild(1).getValue());
            pro.setContacttitle(node.getChild(3).getValue());
            pro.setCompanyname(node.getChild(5).getValue());
            pro.setAddress(node.getChild(7).getValue());
            pro.setCity(node.getChild(9).getValue());
            pro.setPostalcode(Integer.parseInt(node.getChild(11).getValue()));
            pro.setCountry(node.getChild(13).getValue());
            pro.setPhone(Integer.parseInt(node.getChild(15).getValue()));
            pro.setFax(Integer.parseInt(node.getChild(17).getValue()));
            arrayList.add(pro);
        }
        return arrayList;
    }
}
