/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Puuuuu
 */
public class CustomerDataAccess {
    protected Connection conn;
    public CustomerDataAccess(){
     try {
           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String user = "sa";
            String pass = "123456";
            String url = "jdbc:sqlserver://localhost;databaseName=INTXML";
            try {
                conn = DriverManager.getConnection(url,user, pass );
            } catch (SQLException ex) {
            }
        } catch (ClassNotFoundException ex) {
        }
    }
    public ResultSet getData(){
    ResultSet rs = null;  
        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from Customer");
        } catch (SQLException ex) {
           
        }
        return rs;
    }
    
    public void writeXML(String url) throws ParserConfigurationException, SQLException, TransformerConfigurationException, TransformerException, IOException{
     DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.newDocument();
        Element root = document.createElement("Customers");
        document.appendChild(root);
        ResultSet rs = this.getData();
        while(rs.next()){
            Element child = document.createElement("Customer");
            root.appendChild(child);
            Element Contactname = document.createElement("Contactname");
            Contactname.setTextContent(rs.getString("Contactname"));
            child.appendChild(Contactname);
            Element Contacttitle = document.createElement("Contacttitle");
            Contacttitle.setTextContent(rs.getString("Contacttitle"));
            child.appendChild(Contacttitle);
            Element Companyname = document.createElement("Companyname");
            Companyname.setTextContent(rs.getString("Companyname"));
            child.appendChild(Companyname);
            Element Address = document.createElement("Address");
            Address.setTextContent(rs.getString("Address"));
            child.appendChild(Address);
            Element City = document.createElement("City");
            City.setTextContent(rs.getString("City"));
            child.appendChild(City);
            Element Postalcode = document.createElement("Postalcode");
            Postalcode.setTextContent(rs.getString("Postalcode"));
            child.appendChild(Postalcode);
            Element Country = document.createElement("Country");
            Country.setTextContent(rs.getString("Country"));
            child.appendChild(Country);
            Element Phone = document.createElement("Phone");
            Phone.setTextContent(rs.getString("Phone"));
            child.appendChild(Phone);
            Element Fax = document.createElement("Fax");
            Fax.setTextContent(rs.getString("Fax"));
            child.appendChild(Fax);
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"no");
        transformer.setOutputProperty(OutputKeys.STANDALONE,"yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING,"iso-8859-1");
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(document);
        transformer.transform(source, result);
        FileWriter fwriter = new FileWriter(new File(url + "Customers.xml"));
        fwriter.write(writer.toString());
        fwriter.close();
}
}
