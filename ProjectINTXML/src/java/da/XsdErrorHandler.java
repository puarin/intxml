/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import nu.xom.Builder;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
/**
 *
 * @author Puuuuu
 */
public class XsdErrorHandler {

    public static StringBuilder validate(String xml, String xsd) {
        StringBuilder str = new StringBuilder();
        try {
            DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = parser.parse(new File(xml));

            Schema schema = SchemaFactory.
                    newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).
                    newSchema(new StreamSource(new File(xsd)));
            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(doc));
            System.out.println("valid");

        } catch (ParserConfigurationException ex) {
            str.append(ex.getMessage());
        } catch (SAXException ex) {
            str.append(ex.getMessage());
        } catch (IOException ex) {
            str.append(ex.getMessage());
        }
        return str;

    }
}
